import dearpygui.dearpygui as dpg

def close_win(w):
    fully_destroy(w)

def alert(text):
    with dpg.window(tag="alert_win", modal=True, width=200, no_close=True):
        dpg.add_text(text)
        dpg.add_button(label="OK", callback=lambda: fully_destroy("alert_win"))

def open_win(tag, on_close=close_win):
    with dpg.window(label=tag, tag=tag, on_close=lambda: on_close(tag)): pass

def clear_listbox(name):
    dpg.configure_item(name, items=[])

def append_listbox(name, item):
    items = dpg.get_item_configuration(name)["items"]
    items.append(item)
    dpg.configure_item(name, items=items)

def remove_listbox(name, item):
    items = dpg.get_item_configuration(name)["items"]
    items.remove(item)
    dpg.configure_item(name, items=items)

def fully_destroy(name):
    sub = dpg.get_item_children(name)
    for item in sub:
        for c in sub[item]:
            if dpg.is_item_container(c):
                fully_destroy(c)
            if dpg.does_item_exist(c): dpg.delete_item(c)
    if dpg.does_item_exist(name): dpg.delete_item(name)