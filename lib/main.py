
def _echo(state, line):
    state.writeln("You say...")
    state.writeln(line)

def _move(state, direction):
    if state.statics.is_valid_direction(direction):
        player, location = state.current()
        if location["paths"][direction]["lock"]:
            result = state.call_function(location["paths"][direction]["key"])
            if not result:
                return

        next_zone = state.manager.get_zone(location["paths"][direction]["target"])
        if next_zone != None:
            state.manager.move_object(player, next_zone)
            state.writeln("You moved to "+next_zone['name'])
        else:
            state.writeln("Nothing there.")
    else:
        state.writeln("Move where?")

def _look(state, line):
    player, location = state.current()
    
    #state.writeln(player)
    #state.writeln(location)
    state.writeln("You are "+player['name']+" in "+location['name'])
    items = []

    for tag in location["contents"]:
        item = state.manager.get_object(tag)
        if item["name"] != player["name"]:
            items.append(item["name"])

    if len(items) > 0:
        state.writeln(f"There are {len(items)} things here; {', '.join(items)}")

    for d in location["paths"]:
        zone = location["paths"][d]
        if zone["target"] != "":
            state.writeln("There is "+zone['target']+" to the "+d)
    
    state.emit("looked")

def savestate(state, line):
    if line == "":
        line = "save"
    state.export(state.game_id+"_"+line, "saves")

def loadstate(state, line):
    if line == "":
        line = "save"
    state.load(state.game_id+"_"+line, "saves")

def _fwait(state, line):
    state.writeln("You do nothing.")
    
def fychelp(state, line):
    pass

def _setv(state, *, key, val):
    state.set_var(key, val)
    
def load(state):
    state.set_command("save", savestate)
    state.set_command("load", loadstate)
    state.set_command("help", fychelp)
    state.set_command("echo", _echo)
    state.set_command("look", _look)
    state.set_command("move", _move)
    state.set_command("wait", _fwait)
    state.set_function("set", _setv)