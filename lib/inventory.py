
def inv_pickup(state, line):
    player, location = state.current()
    
    for tag in location["contents"]:
        item = state.manager.get_object(tag)
        if state.manager.match_object(line, item) and item.get("is_inv") == True and item["location"] == location["id"]:
            state.writeln("You take "+item['name'])
            state.emit("item_taken", name = line, item = item)
            state.manager.move_object(item, player)
            return True

    state.writeln("Take what?")
    return False

def inv_drop(state, line):
    player, location = state.current()
    
    for tag in player["contents"]:
        item = state.manager.get_object(tag)
        if state.manager.match_object(line, item) and item.get("is_inv") == True and item.get("can_drop", True) == True and item["location"] == player["id"]:
            state.writeln("You drop "+item['name'])
            state.emit("item_dropped", name=line, item=item)
            state.manager.move_object(item, location)
            return True

    state.writeln("Take what?")
    return False

def inv_inspect(state, line):
    player, location = state.current()
    
    for tag in player["contents"]:
        item = state.manager.get_object(tag)
        if state.manager.match_object(line, item) and item["location"] == player["id"]:
            if item['description'] != "":
                state.writeln(item['description'])
            else:
                state.writeln("No information on this.")
            return True

    state.writeln("Wear what?")
    return False

def look_inventory_hook(state, **args):
    player, location = state.current()
    i = []
    for tag in player["contents"]:
        item = state.manager.get_object(tag)
        i.append(item['name'])
    
    if len(i) > 0: state.writeln(f"You are carrying {', '.join(i)}")

    for eq in player["equipment"]:
        item = state.manager.get_object(player["equipment"][eq])
        state.writeln(eq.replace("_", " ").title()+": "+item['name'])

def equip(state, item):
    player, location = state.current()
    item = state.manager.get_object(item)
    item["can_drop"] = False
    state.writeln("You wear "+item['name'])
    state.emit("item_equipped", name=item['name'], item=item)
    player["equipment"][item["slot"]] = item["id"]

def inv_wear(state, line):
    player, location = state.current()
    
    for tag in player["contents"]:
        item = state.manager.get_object(tag)
        if state.manager.match_object(line, item) and item.get("wearable") == True and item.get("slot", None) != None and item["location"] == player["id"]:
            state.call_function("equip", item=item)
            return True

    state.writeln("Wear what?")
    return False

def inv_unwear(state, line):
    player, location = state.current()
    

    return False

def is_wearing(state, *, item = "", name = ""):
    #TODO add search for name option
    player, location = state.current()
    item = state.manager.get_object(item)

    if player["equipment"].get(item['slot'], "") == "":
        return False
    return player["equipment"][item["slot"]] == item["id"]

def has_item(state, *, item = "", name = ""):
    #TODO add search for name option
    player, location = state.current()

def load(state):
    print("Loaded inventory library")
    state.set_command("take", inv_pickup)
    state.set_command("drop", inv_drop)
    state.set_command("wear", inv_wear)
    state.set_command("remove", inv_unwear)
    state.set_command("inspect", inv_inspect)
    state.on("looked", look_inventory_hook)

    state.set_function("is wearing?", is_wearing)
    state.set_function("equip", equip)
    state.manager.patch("equipment", dict, _type = "object")
