import sys, os, fyction, json
APP_PATH = os.path.dirname(os.path.realpath(__file__))+"/"
sys.path.append(APP_PATH)
sys.path.append(APP_PATH+"python/")
import dearpygui.dearpygui as dpg
from utils import *
VIEWPORT_WIDTH = 1200
VIEWPORT_HEIGHT = 700
F_STATE = fyction.Fyction()
selected_object = None
selected_zone = None
opened_path = None
last_clicked_text_box = None
dpg.create_context()
dpg.configure_app(init_file=APP_PATH+"windows.ini")
dpg.create_viewport(title='Fyction World Builder', width=VIEWPORT_WIDTH, height=VIEWPORT_HEIGHT)
dpg.setup_dearpygui()
unsaved_edits = False
ctrl_mod = False
alt_mod = False

def title_update():
    title = "Fyction ($)".replace("$", opened_path)
    if unsaved_edits: title = title+" *"
    dpg.set_viewport_title(title)

def edit(unsaved = True):
    global unsaved_edits
    unsaved_edits = unsaved
    title_update()

def update_from_state():
    clear_listbox("zone_list")
    clear_listbox("object_list")
    clear_listbox("scene_list")

    for scene in F_STATE.scene.scenes():
        append_listbox("scene_list", scene)

    for zone in F_STATE.manager.zone_list():
        append_listbox("zone_list", zone["id"])

    for obj in F_STATE.manager.object_list():
        append_listbox("object_list", obj["id"])

    if selected_object != None:
        z = F_STATE.manager.get_object(selected_object)
        dpg.set_value("code_editor_obj", json.dumps(z, indent=2))
    else:
        dpg.set_value("code_editor_obj", "")

    if selected_zone != None:
        z = F_STATE.manager.get_zone(selected_zone)
        dpg.set_value("code_editor_zone", json.dumps(z, indent=2))
    else:
        dpg.set_value("code_editor_zone", "")
    edit()

    

def new_state():
    global opened_path, selected_zone, selected_object
    selected_object = None
    selected_zone = None
    opened_path = "None"
    F_STATE.manager.new()
    update_from_state()
    edit(False)

def open_load_ui():
    def do_load(w, name):
        load(name)
        fully_destroy("load_ui")

    with dpg.window(tag="load_ui", modal=True, width=200):
        dpg.add_listbox(tag="stories", callback=do_load, num_items=18)
        for filename in os.listdir(APP_PATH+"games/"):
            append_listbox("stories", filename.split(".")[0])

def open_save_ui():
    def do_save():
        global opened_path
        name = dpg.get_value("filename")
        opened_path = name
        save_as(name)
        fully_destroy("save_ui")

    with dpg.window(tag="save_ui", modal=True, width=200):
        with dpg.group(horizontal=True):
            dpg.add_input_text(tag="filename", hint="Story name")
            dpg.add_button(label="Save", tag="savefile", callback=lambda: do_save())

        dpg.focus_item("filename")

def load(name):
    global opened_path

    F_STATE.load(name)
    opened_path = name

    update_from_state()
    title_update()
    edit(False)

def save_as(path):
    dpg.save_init_file(APP_PATH+"windows.ini")
    F_STATE.export(path)
    edit(False)

def save():
    global opened_path
    if opened_path != None:
        save_as(opened_path)
    else:
        open_save_ui()

def open_save_ui():
    def do_save():
        name = dpg.get_value("filename")
        save_as(name)
        fully_destroy("save_ui")

    with dpg.window(tag="save_ui", modal=True, width=200):
        with dpg.group(horizontal=True):
            dpg.add_input_text(tag="filename", hint="Story name")
            dpg.add_button(label="Save", tag="savefile", callback=lambda: do_save())

selected_scene = None
selected_passage = None

def clicked_obj(w, item):
    global selected_object, selected_zone, selected_scene, selected_passage

    if w == "passage_list":
        selected_passage = item
        s = F_STATE.scene.get_scene(selected_scene)
        dpg.set_value("scene_text", json.dumps(s[selected_passage], indent=2))
        dpg.set_value("passage_selected", selected_passage)

    if w == "scene_list":
        selected_scene = item
        dpg.set_value("scene_selected", selected_scene)
        s = F_STATE.scene.get_scene(item)
        clear_listbox("passage_list")
        for psg in s.keys():
            append_listbox("passage_list", psg)

    if w == "zone_list":
        selected_zone = item
        z = F_STATE.manager.get_zone(item)
        dpg.set_value("code_editor_zone", json.dumps(z, indent=2))
        dpg.set_value("zone_selected", selected_zone)
        if dpg.does_item_exist("infowin_zone"):
            open_infowin_zone()

    if w == "object_list":
        selected_object = item
        z = F_STATE.manager.get_object(item)
        dpg.set_value("code_editor_obj", json.dumps(z, indent=2))
        dpg.set_value("obj_selected", selected_object)
        if dpg.does_item_exist("infowin_obj"):
            open_infowin_obj()

def callback_editors(w, item):
    print(w, item)
    if w == "scene_text" and (selected_passage == None or selected_scene == None):
        dpg.set_value("valid_json_scene", "No selected scene")
        return

    if w == "code_editor_obj" and selected_object == None:
        dpg.set_value("valid_json_obj", "No selected object")
        return

    if w == "code_editor_zone" and selected_zone == None:
        dpg.set_value("valid_json_zone", "No selected object")
        return  

    try:
        js = json.loads(item)

        if w == "scene_text":
            if dpg.get_value("valid_json_scene") != "OK":
                dpg.set_value("valid_json_scene", "OK")
            F_STATE.scene.set_passage(selected_scene, selected_passage, js)
            edit(True)    

        if w == "code_editor_obj":
            if dpg.get_value("valid_json_obj") != "OK":
                dpg.set_value("valid_json_obj", "OK")
            obj = F_STATE.manager.get_object(selected_object)
            obj.clear()
            for k, v in js.items():
                obj[k] = v
            edit(True)    

        if w == "code_editor_zone":
            if dpg.get_value("valid_json_zone") != "OK":
                dpg.set_value("valid_json_zone", "OK")
            obj = F_STATE.manager.get_zone(selected_zone)
            obj.clear()
            for k, v in js.items():
                obj[k] = v
            edit(True)   

    except json.decoder.JSONDecodeError:
        if w == "scene_text":
            if dpg.get_value("valid_json_scene") != "INVALID":
                dpg.set_value("valid_json_scene", "INVALID") 
        if w == "code_editor_obj":
            if dpg.get_value("valid_json_obj") != "INVALID":
                dpg.set_value("valid_json_obj", "INVALID")
        if w == "code_editor_zone":
            if dpg.get_value("valid_json_zone") != "INVALID":
                dpg.set_value("valid_json_zone", "INVALID")


def open_infowin_obj():
    pos = [0, 19]
    if dpg.does_item_exist("infowin_obj"):
        pos = dpg.get_item_state("infowin_obj")['pos']
        fully_destroy("infowin_obj")

    if selected_object != None:
        with dpg.window(pos = pos, tag="infowin_obj", label=selected_object, on_close=lambda: fully_destroy("infowin_obj")):
            fobj = F_STATE.manager.get_object(selected_object)
            dpg.add_listbox(items=fobj['contents'], width=100, num_items=18)

def open_infowin_zone():
    pos = [0, 19]
    if dpg.does_item_exist("infowin_zone"):
        pos = dpg.get_item_state("infowin_zone")['pos']
        fully_destroy("infowin_zone")

    if selected_zone != None:
        with dpg.window(pos = pos, tag="infowin_zone", label=selected_zone, on_close=lambda: fully_destroy("infowin_zone")):
            dpg.add_button(label=selected_zone)

with dpg.window(tag="scene_editor", label="Scenes", no_close=True):
    with dpg.group(horizontal=True):
        dpg.add_listbox(tag="scene_list", width=100, num_items=20, callback=clicked_obj)
        dpg.add_listbox(tag="passage_list", width=100, num_items=20, callback=clicked_obj)

        with dpg.group():
            with dpg.group(horizontal=True):
                def do_add_scene():
                    if dpg.get_value("new_scene_input") != "":
                        F_STATE.scene.new_scene(dpg.get_value("new_scene_input"))
                        clear_listbox("scene_list")

                        for scene in F_STATE.scene.scenes():
                            append_listbox("scene_list", scene)
                    else:
                        alert("No scene name given.")
                dpg.add_button(label="New scene", width=100, callback=lambda: do_add_scene())
                dpg.add_input_text(tag="new_scene_input", hint="Name of new scene.")
            with dpg.group(horizontal=True):
                dpg.add_button(label="New passage", width=100)
                dpg.add_input_text(tag="new_passage_input", hint="Name of new passage.")

            dpg.add_separator()

            with dpg.group():
                dpg.add_button(tag="new_text_btn", label="New text", width=100)
                dpg.add_input_text(tag="new_text_source", hint="Source of text. (default: system)")
                dpg.add_input_text(tag="new_text_value", hint="Line of text.")

            dpg.add_separator()

            with dpg.group():
                dpg.add_button(tag="new_opt_btn", label="New option", width=100)
                dpg.add_input_text(tag="new_option_target", hint="Passage target")
                dpg.add_input_text(tag="new_option_text", hint="Display text")
                dpg.add_input_text(tag="new_option_call", hint="Function call (e.g. equip|weapon:sword)")
                dpg.add_input_text(tag="new_option_run", hint="Run command (e.g. move north)")

            dpg.add_separator()
            
            dpg.add_input_text(tag="scene_text", multiline=True, width=250, height=300, callback=callback_editors)

            with dpg.group(horizontal=True):
                dpg.add_text(tag="scene_selected")
                dpg.add_text(tag="passage_selected")
                dpg.add_text(tag="valid_json_scene")
                dpg.set_value("valid_json_scene", "OK")

with dpg.window(tag="obj_browser", label="Objects", no_close=True, width=250):
    with dpg.group(horizontal=True):
        with dpg.group():
            def insert_it_obj():
                print(last_clicked_text_box, selected_object)
                dpg.set_value(last_clicked_text_box, selected_object)
                
            dpg.add_listbox(tag="object_list", callback=clicked_obj, width=100, num_items=18)
            with dpg.group(horizontal=True):
                dpg.add_button(label=" ? ", callback=lambda: open_infowin_obj())
                dpg.add_button(label=" > ", callback=lambda: insert_it_obj())

        with dpg.group():
            with dpg.group(horizontal=True, label="Creation"):
                def create_obj():
                    if dpg.get_value("create_id_obj"):
                        F_STATE.manager.create_object(dpg.get_value("create_id_obj"))
                        update_from_state()
                        dpg.set_value("create_id_obj", "")
                    else:
                        alert("An ID is needed.")
                dpg.add_button(label="Create", width=50, callback=lambda: create_obj())
                dpg.add_input_text(tag="create_id_obj", hint="New object ID", width=100, callback=lambda:create_obj(), on_enter=True)

            with dpg.group(horizontal=True, label="Move to"):
                def move_obj():
                    if dpg.get_value("move_obj_to"):
                        if selected_object == None:
                            return alert("No object selected to move.")
                        F_STATE.manager.move_object(selected_object, dpg.get_value("move_obj_to"))
                        update_from_state()
                        dpg.set_value("move_obj_to", "")
                    else:
                        alert("Select a zone and an object from each list.")

                dpg.add_button(label="Move", width=50, callback=lambda: move_obj())
                dpg.add_input_text(tag="move_obj_to", hint="Container ID", width=100, callback=lambda:move_obj(), on_enter=True)   
                
            dpg.add_input_text(tag="code_editor_obj", multiline=True, width=250, height=200, callback=callback_editors)
            with dpg.group(horizontal=True):
                dpg.add_text(tag="obj_selected")
                dpg.add_text(tag="valid_json_obj")
                dpg.set_value("valid_json_obj", "OK")

with dpg.window(tag="zone_browser", label="Zones", no_close=True, width=250):
    with dpg.group(horizontal=True):
        with dpg.group():
            def insert_it():
                dpg.set_value(last_clicked_text_box, selected_zone)
            
            dpg.add_listbox(tag="zone_list", callback=clicked_obj, width=100, num_items=18)
            with dpg.group(horizontal=True):
                dpg.add_button(label=" ? ", callback=lambda: open_infowin_zone())
                dpg.add_button(label=" > ", callback=lambda: insert_it())

        with dpg.group():
            with dpg.group(horizontal=True):
                def create_obj():
                    if dpg.get_value("create_id_zone"):
                        F_STATE.manager.create_zone(dpg.get_value("create_id_zone"))
                        update_from_state()
                        dpg.set_value("create_id_zone", "")
                    else:
                        alert("An ID is needed.")

                dpg.add_button(label="Create", width=50, callback=lambda: create_obj())
                dpg.add_input_text(tag="create_id_zone", hint="New zone ID", width=100, callback=lambda:create_obj(), on_enter=True)

            with dpg.group(horizontal=True):
                def create_obj():
                    if dpg.get_value("obj_in_zone_spawn") and selected_zone:
                        F_STATE.manager.spawn_object(dpg.get_value("obj_in_zone_spawn"), selected_zone)
                        update_from_state()
                        dpg.set_value("obj_in_zone_spawn", "")
                    else:
                        alert("An ID is needed.")
                        
                dpg.add_button(label="Spawn", width=50, callback=lambda: create_obj())
                dpg.add_input_text(tag="obj_in_zone_spawn", hint="New object ID", width=100, callback=lambda: create_obj(), on_enter=True)

            dpg.add_input_text(tag="code_editor_zone", multiline=True, width=250, height=200, callback=callback_editors)
            with dpg.group(horizontal=True):
                dpg.add_text(tag="zone_selected")
                dpg.add_text(tag="valid_json_zone")
                dpg.set_value("valid_json_zone", "OK")

                

with dpg.viewport_menu_bar():
    with dpg.menu(label="File", tag="menu_file"):
        dpg.add_menu_item(label="New", callback=lambda: new_state())
        dpg.add_menu_item(label="Load", callback=lambda: open_load_ui())
        dpg.add_menu_item(label="Save", callback=lambda: save())
        dpg.add_menu_item(label="Save as...", callback=lambda: open_save_ui())
        dpg.add_separator()
        dpg.add_menu_item(label="Create Object")
        dpg.add_menu_item(label="Create Zone")

HELP_D = {
    "Main": "This is the main help heading.",
    "Keyboard": """ = Keyboard = 
F1 = Help
Ctrl-S = save
    """
}

def open_help(subheading = None):
    with dpg.window(label = "Help", tag="help menu", on_close=lambda: fully_destroy("help menu")):
        with dpg.group(parent="help menu", horizontal=True):
            dpg.add_group(tag="help_nav")
            dpg.add_input_text(tag="help_text", width=300, height=400, multiline=True)

        def set_help(w):
            lbl = dpg.get_item_label(w)
            dpg.set_value("help_text", HELP_D[lbl])

        for heading, body in HELP_D.items():
            dpg.add_button(label = heading, parent="help_nav", callback=set_help, width=100)

        if subheading != None: dpg.set_value("help_text", HELP_D[subheading])

        dpg.bind_item_handler_registry("help menu", "whand")


def handle_key_input(w, key):
    global alt_mod, ctrl_mod
    if dpg.does_item_exist("alert_win"):
        fully_destroy("alert_win")
        return

    if key == 290:
        if dpg.does_item_exist("help menu"):
            dpg.focus_item("help menu")
        else:
            open_help()
    if key == 291:
        pass #new
    if key == 292:
        pass #load
    if key == 293:
        pass #save
    if key == 294:
        pass #save
    if key == 257:
        if dpg.does_item_exist("save_ui"):
            name = dpg.get_value("filename")
            save_as(name)
            fully_destroy("save_ui")
    if key == 341:
        ctrl_mod = True

    if key == 83 and ctrl_mod:
        save()

def handle_keyr_input(w, key):
    global alt_mod, ctrl_mod
    if key == 341:
        ctrl_mod = False

with dpg.handler_registry(): 
    dpg.add_key_press_handler(callback=handle_key_input)
    dpg.add_key_release_handler(callback=handle_keyr_input)


def resize_story(w, i):
    if i == "scene_editor":
        dpg.set_item_width("new_opt_btn", dpg.get_item_width("scene_editor")-233)
        dpg.set_item_width("new_text_btn", dpg.get_item_width("scene_editor")-233)
        dpg.set_item_width("new_scene_input", dpg.get_item_width("scene_editor")-340)
        dpg.set_item_width("new_passage_input", dpg.get_item_width("scene_editor")-340)
        dpg.set_item_width("new_option_target", dpg.get_item_width("scene_editor")-233)
        dpg.set_item_width("new_option_text", dpg.get_item_width("scene_editor")-233)
        dpg.set_item_width("new_option_call", dpg.get_item_width("scene_editor")-233)
        dpg.set_item_width("new_option_run", dpg.get_item_width("scene_editor")-233)

        dpg.set_item_width("new_text_source", dpg.get_item_width("scene_editor")-233)
        dpg.set_item_width("new_text_value", dpg.get_item_width("scene_editor")-233)

        dpg.set_item_width("scene_text", dpg.get_item_width("scene_editor")-233)
        dpg.set_item_height("scene_text", dpg.get_item_height("scene_editor")-300)

    if i == "zone_browser":
        dpg.set_item_width("code_editor_zone", dpg.get_item_width("zone_browser")-137)
        dpg.set_item_height("code_editor_zone", dpg.get_item_height("zone_browser")-105)  
        dpg.set_item_width("create_id_zone", dpg.get_item_width("zone_browser")-195)
        dpg.set_item_width("obj_in_zone_spawn", dpg.get_item_width("zone_browser")-195)
        
    if i == "obj_browser":
        dpg.set_item_width("code_editor_obj", dpg.get_item_width("obj_browser")-137)
        dpg.set_item_height("code_editor_obj", dpg.get_item_height("obj_browser")-105)  
        dpg.set_item_width("create_id_obj", dpg.get_item_width("obj_browser")-195)
        dpg.set_item_width("move_obj_to", dpg.get_item_width("obj_browser")-195)

with dpg.item_handler_registry(tag="whand") as f: 

    dpg.add_item_resize_handler(callback=resize_story)

with dpg.item_handler_registry(tag="clickhandle") as f: 
    def refocus(w, i, n):
        global last_clicked_text_box
        last_clicked_text_box = i[1]
    dpg.add_item_clicked_handler(callback=refocus)

"""
with dpg.item_handler_registry(tag="ctxmenu") as f: 
    def open_ctx(w, i, n):
        with dpg.window(tag="ctx_menu", on_close=lambda: fully_destroy("ctx_menu")):
            def insert_it():
                print(last_clicked_text_box, selected_object)
                dpg.set_value(last_clicked_text_box, selected_object)
                fully_destroy("ctx_menu")

            dpg.add_button(label="Copy to text box", callback=lambda: insert_it())

    dpg.add_item_clicked_handler(button=1, callback=open_ctx)
"""
if __name__ == "__main__":

    dpg.show_viewport()
    dpg.bind_item_handler_registry("zone_browser", "whand")
    dpg.bind_item_handler_registry("obj_browser", "whand")
    dpg.bind_item_handler_registry("scene_editor", "whand")
    dpg.bind_item_handler_registry("obj_in_zone_spawn", "clickhandle")
    dpg.bind_item_handler_registry("create_id_obj", "clickhandle")
    dpg.bind_item_handler_registry("move_obj_to", "clickhandle")
    dpg.bind_item_handler_registry("create_id_zone", "clickhandle")
    dpg.bind_item_handler_registry("zone_list", "ctxmenu")
    dpg.bind_item_handler_registry("object_list", "ctxmenu")
    if len(sys.argv) > 1:
        load(sys.argv[1])
    dpg.start_dearpygui()

    dpg.destroy_context()