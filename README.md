# Fyction
Fyction is a concept engine for parsing, creating, and playing classic text-adventure interactive fyction games that are written as JSON notation and run in a pure python environment, allowing for high levels of extendability.<br>
The engine is also designed to be flexible enough for authors to write their own custom interfaces. By default, Fyction runs a simple command-line interface.<br>

## Specification
Details of how each part of the game world is structured.

### The Game
Each game is stored as a simple json state. Whenever the game world changes, the json values change.<br>
When you save the game, a copy of the current complete json state is stored as a save file.

```json
{
    //The list of objects, "things" in the world
    "objects": [],
    //The list of zones, "places" in the world
    "zones": [],

    //Any story variables that the author can store and recall, for flow control or anything else
    "variables": {},

    "id": "",
    //Libraries are additional python scripts that authors can bundle with their story.
    //They can add additional commands, register events, modify the state, anything that the author may require.
    //Main library should always be included as it contains core functionality
    "libraries": ["main"],

    //List of scenes, essentially dialog trees
    "scenes": {},

    //Metadata
    "author": "",
    "name": "",
    "intro": ""
}
```

### Objects
```json
{
    // The name of the object, displayed in the world
    "name": "",
    //Unique tag of the object, how the author can find specific items in the world
    "id": "",
    //A list of tags that are considered "inside" this object. Useful for containers, the player inventory, etc.
    "contents": [],
    //Information of the object.
    "description": "",

    //Where this object is in the world. Shouldn't be edited manually unless you also make sure to update the respective zones contents.
    "location": "",

    //Just a value to tell the engine that this is an object
    "type": "object"
}
```
### Zones
```json
{
    //Same as Object
    "name": "",
    "id": "",
    "description": "",

    //List of ID's of things currently in this zone
    "contents": [],

    //Paths are what tells the engine how zones connect to eachother to allow the player to traverse the world with the `move` commands.

    "paths": {
        "north": {"lock": False, "target": "", "description": "", "key": ""},
        "west": {"lock": False, "target": "", "description": "", "key": ""},
        "east": {"lock": False, "target": "", "description": "", "key": ""},
        "south": {"lock": False, "target": "", "description": "", "key": ""},
        "up": {"lock": False, "target": "", "description": "", "key": ""},
        "down": {"lock": False, "target": "", "description": "", "key": ""},
        "in": {"lock": False, "target": "", "description": "", "key": ""},
        "out": {"lock": False, "target": "", "description": "", "key": ""},
    },

    "type": "zone"
}
```
### Scenes
Scenes are a simple structure to enable branching paths.<br>
In the text array, this is what the engine prints to the users.<br>
Printed text has the format of;<br>
<source>: <body><br>
Text itself can be a string, or list.
If string, it is printed in its entirety as one block with `scene` as the source.<br>
If a list, each entry can then be a string, list, or dict.<br>
If the entry is a string, it is printed as it's own line.<br>
If the entry is a list, index 0 is used as the source and 1 is the text.<br>
If the entry is a dict, key's are `text` and `source`.<br>

#### Options
In the options list, which the user can choose the responses from, we support `check`, `run` and `call` keys for extra functionality.<br>
Check allows controlling if an option is available.<br>
Run executes actions, any command the player can input normally is allowed here, can be used for things like moving the player onward a zone, picking up an item, etc.<br>
Call gives access to registered functions, e.i. library extensions, such as the inventory libraries "equip" function. If an argument is required, split the name and the argument with |.<br>

#### Conditionals


```json
{
    "start": {
        "text": [
            ["Paul Denton", "Paul! I thought you were in Hong Kong."],
            ["", "Welcome to the Coalition, JC... I might as well start using your codename. Think I'd miss my brother's first day?"],
            ["", "Didn't think you'd have a choice. What's going on?"],
            ["", "The NSF -- they hit one of our shipments. A few of them got away, but we trapped the rest in the Statue."],
            ["", "What are we waiting for? Looks like a textbook assault."],
            ["", "The NSF took one of our agents hostage. The bots are holding the perimeter, but my orders are to hold back and send you in alone. I think someone high up wants to see how you handle the situation."],
            ["", "All I've got with me is a pistol and an electric prod. I don't mind a test, but UNATCO better issue some hardware."],
            ["", "Remember that we're police. Stick with the prod. It will stun your opponents or knock them unconscious. A nonlethal takedown is always the most silent way to eliminate resistance. Just in case, though, Manderley wants you to pick an additional weapon: a sniper rifle, a GEP gun, or a minicrossbow."]
            ],
        "options": [
            {"target": "part_2", "text": "I like to pick 'em off from a distance. I'll take the rifle.", "call": "dock_choice|rifle"},
            {"target": "part_2", "text": "Never know when I might come up against heavy armor. Give me the GEP gun.", "call": "dock_choice|gep"},
            {"target": "part_2", "text": "The crossbow. Sometimes you've got to make a silent takedown.", "call": "dock_choice|crossbow"}
        ]
    },
    "part_2": {
        "text": [
            ["Paul Denton", "This isn't a training exercise, JC. Your targets will be human beings. Keep that in mind.", "choice is rifle"],
            ["Paul Denton", "Good thinking. With these tranquilizer darts, you'll have another nonlethal way to take down an enemy in addition to the prod.", "choice is crossbow"],
            ["Paul Denton", "The GEP gun might be useful. They have a security bot on patrol near the Statue entrance.", "choice is gep"],
        ],
        "options": [
            {"target": "__end", "text": "Bye"},
            {"target": "__end", "text": "This is my new lockpick.", "check": "choice is gep"}
        ]
    }
}
```

### Config
The engine has it's own config settings to control how things work.<br>
Currently supported is aliases.
Aliases allow shortcuts for commands and inputs, stored as key/value pairs.<br>
They can be used in two ways, if whole input IS the key, then the value is parsed as the users input.<br>
The value can also be inserted in the middle of messages by sending $key in the text.<br>
E.g.;<br>
> n <br>
becomes <br>
> move north <br>
while <br>
> echo $n <br>
becomes  <br>
> echo move north <br>

```json
{
    "aliases": {
        "n": "move north",
        "s": "move south"
    }
}
```