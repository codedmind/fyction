import sys, os, fyction, json
APP_PATH = os.path.dirname(os.path.realpath(__file__))+"/"
sys.path.append(APP_PATH)
sys.path.append(APP_PATH+"python/")
import dearpygui.dearpygui as dpg

dpg.create_context()

with dpg.window(tag="Primary Window"):
    dpg.add_text("Welcome to Fyction.")
    dpg.add_button(label="Player", width=200)
    dpg.add_button(label="Editor", width=200)

dpg.create_viewport(title='Custom Title', width=600, height=200)
dpg.setup_dearpygui()
dpg.show_viewport()
dpg.set_primary_window("Primary Window", True)
dpg.start_dearpygui()
dpg.destroy_context()