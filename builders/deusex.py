def builder(f, manager):
        manager.set_id("deusex")
        f.link_library("inventory")
        f.link_library("deusex")
        manager.create_zone("Dock", description = "The docks.")
        manager.create_zone("Island: South")
        manager.link_zones("Dock", "Island: South", "north")
        manager.spawn_object("JC Denton", "Dock")
        manager.spawn_object("glock1", "Dock", ammo = 6, name = "Pistol", is_inv = True)
        manager.spawn_object("trench_coat", "Dock", wearable = True, name = "Trench Coat", is_inv = True, slot = "armour")
        f.focus("JC Denton")
        
        
        manager.set_author("Kaiser")
        manager.set_name("Deus Ex: DE-Make")
        manager.set_intro("You depart from your boat on to the docks of Liberty Island.")
        
        manager.register_scene("paul_docks", {
            "start": {
                "text": [
                    ["Paul Denton", "Paul! I thought you were in Hong Kong."],
                    ["", "Welcome to the Coalition, JC... I might as well start using your codename. Think I'd miss my brother's first day?"],
                    ["", "Didn't think you'd have a choice. What's going on?"],
                    ["", "The NSF -- they hit one of our shipments. A few of them got away, but we trapped the rest in the Statue."],
                    ["", "What are we waiting for? Looks like a textbook assault."],
                    ["", "The NSF took one of our agents hostage. The bots are holding the perimeter, but my orders are to hold back and send you in alone. I think someone high up wants to see how you handle the situation."],
                    ["", "All I've got with me is a pistol and an electric prod. I don't mind a test, but UNATCO better issue some hardware."],
                    ["", "Remember that we're police. Stick with the prod. It will stun your opponents or knock them unconscious. A nonlethal takedown is always the most silent way to eliminate resistance. Just in case, though, Manderley wants you to pick an additional weapon: a sniper rifle, a GEP gun, or a minicrossbow."],
                    ["", ""],
                    ["", ""],
                    ["", ""],
                    ],
                "options": [
                    {"target": "part_2", "text": "I like to pick 'em off from a distance. I'll take the rifle.", "call": "dock_choice|rifle"},
                    {"target": "part_2", "text": "Never know when I might come up against heavy armor. Give me the GEP gun.", "call": "dock_choice|gep"},
                    {"target": "part_2", "text": "The crossbow. Sometimes you've got to make a silent takedown.", "call": "dock_choice|crossbow"}
                ]
            },
            "part_2": {
                "text": [
                    ["Paul Denton", "This isn't a training exercise, JC. Your targets will be human beings. Keep that in mind.", "choice is rifle"],
                    ["Paul Denton", "Good thinking. With these tranquilizer darts, you'll have another nonlethal way to take down an enemy in addition to the prod.", "choice is crossbow"],
                    ["Paul Denton", "The GEP gun might be useful. They have a security bot on patrol near the Statue entrance.", "choice is gep"],
                ],
                "options": [
                    {"target": "__end", "text": "Bye"},
                    {"target": "__end", "text": "This is my new lockpick.", "check": "choice is gep"}
                ]
            }
        })