import sys, os, fyction, json
APP_PATH = os.path.dirname(os.path.realpath(__file__))+"/"
sys.path.append(APP_PATH)
sys.path.append(APP_PATH+"python/")
import dearpygui.dearpygui as dpg
from utils import *
VIEWPORT_WIDTH = 1200
VIEWPORT_HEIGHT = 700
F_STATE = fyction.Fyction()
selected_object = None
selected_zone = None
opened_path = None
last_clicked_text_box = None
dpg.create_context()
dpg.configure_app(init_file=APP_PATH+"windows.ini")
dpg.create_viewport(title='Fyction Player', width=VIEWPORT_WIDTH, height=VIEWPORT_HEIGHT)
dpg.setup_dearpygui()
ctrl_mod = False
alt_mod = False
clicked_cmd = ""
cmd_saved = ""
stored_item_locs = {}

def tick():
    global clicked_cmd, cmd_saved
    for bf in F_STATE.buffer:
        dpg.set_value("logbox", f"{bf['source']}: {bf['text']}\n{dpg.get_value('logbox')}")

    F_STATE.clear_buffer()
    update_items()
    update_inventory()
    clicked_cmd = ""
    cmd_saved = ""
    
def title_update():
    title = "Fyction ($)".replace("$", opened_path)
    dpg.set_viewport_title(title)

def update_items():
    if dpg.does_item_exist("arealist"):
        clear_listbox("arealist")
        player, loc = F_STATE.current()
        for item in loc['contents']:
            real = F_STATE.manager.get_object(item)
            append_listbox("arealist", real['name'])

def update_inventory():
    if dpg.does_item_exist("inventory_list"):
        #print(player)
        clear_listbox("inventory_list")
        player, loc = F_STATE.current()
        for item in player['contents']:
            real = F_STATE.manager.get_object(item)
            append_listbox("inventory_list", real['name'])

def update_ui_elements():
    if "inventory" in F_STATE.state["libraries"]:
        dpg.show_item("inv_button")
    else:
        dpg.hide_item("inv_button")

def update_command_list():
    clear_listbox("cmdlist")
    for command in F_STATE.commands.keys():
        append_listbox("cmdlist", command)

def update_from_state():
    update_items()
    update_inventory()
    update_command_list()
    update_ui_elements()



    

def new_state():
    global opened_path
    opened_path = "None"
    F_STATE.manager.new()
    update_from_state()

def open_load_ui(base_path = "games"):
    def do_load(w, name):
        load(name, base_path)
        fully_destroy("load_ui")

    with dpg.window(tag="load_ui", modal=True, width=200):
        dpg.add_listbox(tag="stories", callback=do_load, num_items=18)
        for filename in os.listdir(APP_PATH+base_path+"/"):
            append_listbox("stories", filename.split(".")[0])

def open_save_ui():
    def do_save():
        global opened_path
        name = dpg.get_value("filename")
        opened_path = name
        save_as(name)
        fully_destroy("save_ui")

    with dpg.window(tag="save_ui", modal=True, width=200):
        with dpg.group(horizontal=True):
            dpg.add_input_text(tag="filename", hint="Story name")
            dpg.add_button(label="Save", tag="savefile", callback=lambda: do_save())

        dpg.focus_item("filename")

def load(name, directory):
    global opened_path

    F_STATE.load(name, directory)
    opened_path = name

    update_from_state()
    title_update()
    tick()

def save_as(path):
    dpg.save_init_file(APP_PATH+"windows.ini")
    F_STATE.export(path, "saves")

def save():
    global opened_path
    if opened_path != None:
        save_as(opened_path)
    else:
        open_save_ui()

def open_save_ui():
    def do_save():
        name = dpg.get_value("filename")
        save_as(name)
        fully_destroy("save_ui")

    with dpg.window(tag="save_ui", modal=True, width=200):
        with dpg.group(horizontal=True):
            dpg.add_input_text(tag="filename", hint="Story name")
            dpg.add_button(label="Save", tag="savefile", callback=lambda: do_save())


def clicked_obj(w, item):
    global clicked_cmd, cmd_saved
    if w == "cmdlist":
        clicked_cmd = item
        dpg.set_value("inputbar", clicked_cmd+" "+cmd_saved)
    print(w, item)
    if w == "arealist" or w == "inventory_list":
        
        cmd_saved = item
        dpg.set_value("inputbar", clicked_cmd+" "+cmd_saved)

with dpg.viewport_menu_bar():
    with dpg.menu(label="File", tag="menu_file"):
        dpg.add_menu_item(label="New", callback=lambda: new_state())
        dpg.add_menu_item(label="Load", callback=lambda: open_load_ui())
        dpg.add_menu_item(label="Save", callback=lambda: save())
        dpg.add_menu_item(label="Save as...", callback=lambda: open_save_ui())

HELP_D = {
    "Main": "This is the main help heading.",
    "Keyboard": """ = Keyboard = 
    F1 = Help
    Ctrl-S = save
    """
}

def open_help(subheading = None):
    with dpg.window(label = "Help", tag="help menu", on_close=lambda: fully_destroy("help menu")):
        with dpg.group(parent="help menu", horizontal=True):
            dpg.add_group(tag="help_nav")
            dpg.add_input_text(tag="help_text", width=300, height=400, multiline=True)

        def set_help(w):
            lbl = dpg.get_item_label(w)
            dpg.set_value("help_text", HELP_D[lbl])

        for heading, body in HELP_D.items():
            dpg.add_button(label = heading, parent="help_nav", callback=set_help, width=100)

        if subheading != None: dpg.set_value("help_text", HELP_D[subheading])

        dpg.bind_item_handler_registry("help menu", "whand")

def send_cmd_input():
    text = dpg.get_value("inputbar")
    F_STATE.read(text)
    dpg.set_value("inputbar", "")
    tick()

def handle_key_input(w, key):
    global alt_mod, ctrl_mod

    if dpg.does_item_exist("alert_win"):
        fully_destroy("alert_win")
        return

    if key == 290:
        if dpg.does_item_exist("help menu"):
            dpg.focus_item("help menu")
        else:
            open_help()
    if key == 291:
        pass #new
    if key == 292:
        pass #load
    if key == 293:
        pass #save
    if key == 294:
        pass #save
    if key == 257:
        if dpg.does_item_exist("save_ui"):
            name = dpg.get_value("filename")
            save_as(name)
            fully_destroy("save_ui")
        else:
            send_cmd_input()
    if key == 341:
        ctrl_mod = True

    if key == 83 and ctrl_mod:
        save()

def handle_keyr_input(w, key):
    global alt_mod, ctrl_mod
    if key == 341:
        ctrl_mod = False

with dpg.handler_registry(): 
    dpg.add_key_press_handler(callback=handle_key_input)
    dpg.add_key_release_handler(callback=handle_keyr_input)

def resize_story(w, i):
    if i == "logwin":
        dpg.set_item_width("logbox", dpg.get_item_width("logwin")-15)
        dpg.set_item_height("logbox", dpg.get_item_height("logwin")-40)  

with dpg.item_handler_registry(tag="whand") as f: 
    dpg.add_item_resize_handler(callback=resize_story)

with dpg.item_handler_registry(tag="clickhandle") as f: 
    def refocus(w, i, n):
        global last_clicked_text_box
        last_clicked_text_box = i[1]
    dpg.add_item_clicked_handler(callback=refocus)

def toggle_inventory():
    global stored_item_locs

    if dpg.does_item_exist("inventory_win"):
        stored_item_locs["inventory_win"] = dpg.get_item_state("inventory_win")['pos']
        fully_destroy("inventory_win")
    else:
        pos = [20, 0]
        if stored_item_locs.get("inventory_win", None) != None: pos = stored_item_locs["inventory_win"]
        with dpg.window(tag="inventory_win", label="Inventory", on_close=lambda: fully_destroy("inventory_win"), no_resize=True, width=250, pos=pos):
            dpg.add_listbox(tag="inventory_list", num_items=20, width=235, callback=clicked_obj)
        update_inventory()

def toggle_arealist():
    global stored_item_locs

    if dpg.does_item_exist("zonelist"):
        stored_item_locs["zonelist"] = dpg.get_item_state("zonelist")['pos']
        fully_destroy("zonelist")
    else:
        pos = [20, 0]
        if stored_item_locs.get("zonelist", None) != None: pos = stored_item_locs["zonelist"]
        with dpg.window(tag="zonelist", label="Area", on_close=lambda: fully_destroy("zonelist"), no_resize=True, width=250, pos=pos):
            dpg.add_listbox(tag="arealist", num_items=20, width=235, callback=clicked_obj)
        update_items()

with dpg.window(tag="logwin", label="Log", no_close=True):
    dpg.add_input_text(tag="logbox", multiline=True)

with dpg.window(tag="inputwin", label="Input", no_close=True, no_resize=True, height=40, width=540):
    dpg.add_input_text(tag="inputbar", width=500)
    with dpg.group(horizontal=True):
        dpg.add_button(label="Send", callback=lambda: send_cmd_input())
        dpg.add_button(tag="inv_button", label="Inventory", callback=lambda: toggle_inventory())
        dpg.hide_item("inv_button")
        dpg.add_button(tag="zonelistbtn", label="Area", callback=lambda: toggle_arealist())
        #dpg.add_button(tag="zone_items", label="Area")

        

with dpg.window(tag="cmdwin", label="Commands", no_close=True, no_resize=True, width=250):
    dpg.add_listbox(tag="cmdlist", num_items=20, width=200, callback=clicked_obj)

def quicksend(line):
    F_STATE.read(line)
    tick()

with dpg.window(tag="navwin", label="Navigation", no_close=True, no_resize=True,height=300, width=540):
    with dpg.group(horizontal=True):
        dpg.add_button(label="IN", height=50, width=50, callback=lambda: quicksend("move in"))  
        dpg.add_button(label="NORTH", height=50, width=50, callback=lambda: quicksend("move north"))  
        dpg.add_button(label="UP", height=50, width=50, callback=lambda: quicksend("move up"))  
    with dpg.group(horizontal=True):
        dpg.add_button(label="WEST", height=50, width=50, callback=lambda: quicksend("move west"))  
        dpg.add_button(label="WAIT", height=50, width=50, callback=lambda: quicksend("wait"))  
        dpg.add_button(label="EAST", height=50, width=50, callback=lambda: quicksend("move east"))  
    with dpg.group(horizontal=True):
        dpg.add_button(label="OUT", height=50, width=50, callback=lambda: quicksend("move out"))  
        dpg.add_button(label="SOUTH", height=50, width=50, callback=lambda: quicksend("move south"))  
        dpg.add_button(label="DOWN", height=50, width=50, callback=lambda: quicksend("move down"))

if __name__ == "__main__":
    dpg.show_viewport()
    dpg.bind_item_handler_registry("logwin", "whand")

    if len(sys.argv) > 1:
        load(sys.argv[1], "games")
    dpg.start_dearpygui()

    dpg.destroy_context()