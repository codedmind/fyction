import readline, importlib
from fyction import *

if __name__ == "__main__":
    State = Fyction()
    user = ""
    print("=================================")
    print("= Fyction CLI Interface ALPHA   =")
    print("=================================")
    print("`.help` for information.")
    print("Running from "+State.app_path)

    def override_test(st, ln):
        print("Got "+ln+". Say `end` to leave.")
        if ln == "end":
            st.set_override()

    if len(sys.argv) > 1: 
        if sys.argv[1].startswith("b:"):
            name = sys.argv[1].split(":")[1]
            print("Building "+name)
            f = importlib.import_module("builders."+name)
            f.builder(State, State.manager)
            State.export(name)
            State.load(name)
        else:
            State.load(sys.argv[1])

    while True:
        prompt = State.get_var("__prompt", "")
        user = input("> ")
        if user.startswith("."):
            cmd = user.split(" ")[0][1:]
            parts = user.split(" ")[1:]
            print(cmd, parts)
            if cmd == "set":
                State.set_var(parts[0], parts[1])
            elif cmd == "get":
                print(State.get_var(parts[0]))
            elif cmd == "cset":
                State.set_conf(parts[0], parts[1])
                State.save_config()
            elif cmd == "get":
                print(State.get_conf(parts[0]))
            elif cmd == "help":
                print(".set, .get, .help, .build, .load, .quit")
                print("Interface commands start with a `.`, all other text is sent to the parser.")
            elif cmd == "test":
                State.set_override(override_test)
            elif cmd == "build":
                State.reset()
                f = importlib.import_module("builders."+parts[0])
                f.builder(State, State.manager)
                State.export(parts[0])
                State.load(parts[0])
            elif cmd == "dx":
                State.load("deusex")
            elif cmd == "scene":
                #State.scene.switch_scene("paul_docks")
                m = State.scene
                m.new_scene("custom")
                m.add_passage("custom", "fight")
                m.add_text("custom", "start", "Athena", "You did it!")
                m.add_option("custom", "start", text = "Yay!", target = "fight")
                m.add_text("custom", "fight", "Athena", "It's a fight tho!")
                m.add_option("custom", "fight", text = "I win tho.", target = "__end")
                m.switch_scene("custom")
                
            elif cmd == "list":
                for thingy in State.manager.all_list():
                    print(thingy)
                    print("------------")
            elif cmd == "load":
                State.load(parts[0])
            elif cmd == "quit":
                quit()
        else:
            State.read(user)

        State.echo()
        State.clear_buffer()

