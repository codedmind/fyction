import json, importlib, sys, os, readline
APP_PATH = os.path.dirname(os.path.realpath(__file__))+"/"
sys.path.append(APP_PATH)
sys.path.append(APP_PATH+"python/")

class SceneManager:
    def __init__(self, fyction):
        self.fyction = fyction
        self.current_scene = ""
        self.current_passage = ""
        self.history = []

    def scenes(self):
        return self.fyction.state.get("scenes", {})

    def scene_input(self, state, line):
        s = self.scenes()[self.current_scene]
        run = ""
        call = ""
        if line.isdigit() and int(line) < len(s[self.current_passage]["options"]):
            run = s[self.current_passage]["options"][int(line)].get("run", "")
            call = s[self.current_passage]["options"][int(line)].get("call", "")
            line = s[self.current_passage]["options"][int(line)].get("target", "")
        else:
            for opt in s[self.current_passage]["options"]:
                if opt["text"].lower().startswith(line.lower()):
                    line = opt.get("target", "")
                    run = opt.get("run")
                    call = opt.get("call")
                    break

        if call:
            if "|" in call:
                parts = call.split("|")
                args = {"line": []}
                for a in parts[1:]:
                    if ":" in a:
                        args[a.split(":")[0]] = a.split(":")[1]
                    else:
                        args["line"].append(a)
                args["line"] = " ".join(args['line'])
                state.call_function(parts[0], **args)
            else:
                state.call_function(call)

        if run:
            state.read(run, True)

        if line == "__end":
            self.switch_scene()
            return
        
        if line.startswith("__back"):
            history = 1
            if ":" in line:
                history = int(line.split(":")[1])

            line = self.history[-history]
            self.history = self.history[0:-history]   

        if line != "":
            target = s.get(line, None)
            if target != None:
                self.history.append(self.current_passage)
                self.current_passage = line
                #state.writeln(target["text"])
                self.print_body(state, target)
            else:
                state.writeln("What?", source = "scene")

    def conditional(self, state, ln):
        parts = ln.split(" ")
        #print("cond", ln)
        if len(parts) >= 3:
            value = state.get_var(parts[0])
            comp = parts[1]
            against = " ".join(parts[2:])
            if value != "":
                if comp == "is" or comp == "=" or comp == "==":
                    return value == against

    def print_body(self, state, target):
        if type(target["text"]) == str:
            state.writeln(target["text"], source = "scene")
        elif type(target["text"]) == list:
            for ln in target["text"]:
                if type(ln) == str:
                    source = "scene"
                    state.writeln(ln, source = source)
                if type(ln) == list:
                    if len(ln) == 3:
                        check = self.conditional(state, ln[2])
                        if check == True:
                           state.writeln(ln[1], source = ln[0])
                    else: 
                        state.writeln(ln[1], source = ln[0])
                if type(ln) == dict:
                    if ln.get("check", "") != "":
                        check = self.conditional(state, ln["check"])
                        if check == True:
                            state.writeln(ln["text"], source = ln["source"])
                    else:
                        state.writeln(ln["text"], source = ln["source"])
        i = 0
        for opt in target["options"]:
            if opt.get("check", "") != "":
                check = self.conditional(state, opt["check"])
                if check:
                    state.writeln(opt["text"], source = f"{i} *")
            else:
                state.writeln(opt["text"], source = f"{i} *")
            i += 1

    def get_scene(self, name):
        return self.scenes().get(name, None)
 
    def switch_scene(self, name = None, passage = ""):
        if name == None:
            self.current_scene = ""
            self.current_passage = ""
            self.fyction.set_override()
            return
        if passage == "": 
            passage = "start"
        if name in self.scenes().keys():
            s = self.scenes()[name]
            self.current_scene = name
            self.current_passage = passage
            self.print_body(self.fyction, s[passage])
            self.fyction.set_override(self.scene_input)


    #Editors
    def new_scene(self, name):
        if not self.get_scene(name):
            self.fyction.state['scenes'][name] = self.default_scene()

    def add_passage(self, scene, name):
        self.set_passage(scene, name, self.default_passage())

    def set_passage(self, scene, name, passage_data):
        if self.get_scene(scene):
            self.fyction.state['scenes'][scene][name] = passage_data

    def add_text(self, scene, passage, source, line):
        s = self.get_scene(scene)
        if s and s.get(passage):
            s[passage]['text'].append([source, line])

    def update_text(self, scene, passage, index:int, source, line):
        s = self.get_scene(scene)
        if s and s.get(passage):
            s[passage]['text'][index] = [source, line]

    def add_option(self, scene, passage, **values):
        opt = self.default_option(**values)

        s = self.get_scene(scene)
        if s and s.get(passage):
            s[passage]['options'].append(opt)

    def update_option(self, scene, passage, index, **values):
        opt = self.default_option(**values)

        s = self.get_scene(scene)
        if s and s.get(passage):
            s[passage]['options'][index] = opt

    def default_option(self, **opt):
        psg = {
            "target": "",
            "text": "",
            "call": "",
            "run": ""
        }
        psg.update(opt)
        return psg

    def default_passage(self):
        return {
            "text": [],
            "options": []
        }

    def default_scene(self):    
        return {
            "start": self.default_passage()
        }

class FyctionStatic:
    @staticmethod
    def is_valid_direction(d):
        return d in ["north", "east", "south", "west", "up", "down", "in", "out"]

    @staticmethod
    def invert_direction(d):
        if d == "north":
            return "south"
        elif d == "south":
            return "north"
        elif d == "west":
            return "east"
        elif d == "east":
            return "west"
        elif d == "up":
            return "down"
        elif d == "down":
            return "up"
        elif d == "in":
            return "out"
        elif d == "out":
            return "in"

class StateManager:
    def __init__(self, fyction):
        self.fyction = fyction
        self.save_directory = ""

    def patch(self, key, datatype, **filter_keys):
        """
        Updates objects in the state to add specific keys, then re-exports the state with the changes.
        """
        if filter_keys.get("_type", None) != None:
            filter_keys["type"] = filter_keys["_type"]
            del filter_keys["_type"]

        patches = 0
        for item in self.all_list():
            if item.get(key, None) == None:
                do_patch = True
                for skey in filter_keys:
                    if filter_keys[skey] != item[skey]:
                        do_patch = False

                if do_patch:
                    item[key] = datatype()
                    patches += 1

        if patches > 0:
            print(f"State has been patched {patches} times. (Key {key} as {datatype})")
            self.fyction.export()

    def set_intro(self, text):
        self.fyction.state["intro"] = text

    def set_id(self, text):
        self.fyction.state["id"] = text

    def set_author(self, text):
        self.fyction.state["author"] = text

    def set_name(self, text):
        self.fyction.state["name"] = text

    #TODO add more user-friendly interfaces
    def register_scene(self, name, data):
        self.fyction.state["scenes"][name] = data

    def create_zone(self, id, **args):
        zone = self.default_zone()
        zone["id"] = id
        zone["name"] = id
        zone = zone|args
        self.fyction.state["zones"].append(zone)
        return zone

    def spawn_object(self, id, location, **args):
        newobj = self.create_object(id, **args)
        self.move_object(newobj, location)

    def create_object(self, id, **args):
        obj = self.default_object()
        obj["id"] = id  
        obj["name"] = id
        obj = obj|args
        self.fyction.state["objects"].append(obj)
        return obj

    def all_list(self):
        return self.zone_list()+self.object_list()

    def zone_list(self):
        return self.fyction.state["zones"]

    def get_any(self, id):
        if type(id) == dict: return id
        for an in self.all_list():
            if an["id"].lower().replace(" ", "") == id.lower().replace(" ", ""): return an

    def get_zone(self, id):
        if type(id) == dict: return id
        for zone in self.zone_list():
            if zone["id"].lower().replace(" ", "") == id.lower().replace(" ", ""): return zone

    def object_list(self):
        return self.fyction.state["objects"]

    def get_object(self, id):
        if type(id) == dict: return id
        for obj in self.object_list():
            if obj["id"].lower().replace(" ", "") == id.lower().replace(" ", ""): return obj

    def move_object(self, obj, zone):
        obj = self.get_object(obj)
        zone = self.get_any(zone)
        if obj["location"] != "":
            old_loc = self.get_any(obj["location"])
            old_loc["contents"].remove(obj["id"])
        zone["contents"].append(obj["id"])
        obj["location"] = zone["id"]

    def link_zones(self, one, two, dir_from_one):
        one = self.get_zone(one)
        two = self.get_zone(two)
        
        one["paths"][dir_from_one]["target"] = two["id"]
        two["paths"][FyctionStatic.invert_direction(dir_from_one)]["target"] = one["id"]

    def clean_string(self, name):
        return name.lower().replace(" ", "")

    def match_object(self, search, object):
        search = self.clean_string(search)
        obj = self.get_any(object)
        if search == self.clean_string(obj["name"]) or self.clean_string(obj["name"]).startswith(search) or search == self.clean_string(obj["id"]):
            return True
        
        return False

    def set_zone_lock(self, zone, direction, locked):
        zone = self.get_zone(zone)
        if FyctionStatic.is_valid_direction(direction):
            zone["paths"][direction]["lock"] = locked

    def set_zone_key(self, zone, direction, key):
        zone = self.get_zone(zone)
        if FyctionStatic.is_valid_direction(direction):
            zone["paths"][direction]["key"] = key

    def new(self):
        self.fyction.state = self.default_state()

    def default_state(self):
        return {
            "objects": [],
            "zones": [],
            "author": "",
            "name": "",
            "id": "",
            "variables": {},
            "libraries": ["main"],
            "scenes": {},
            "intro": ""
        }
    
    def default_object(self, **opt):
        obj = {
            "name": "",
            "id": "",
            "contents": [],
            "description": "",
            "events": {},
            "location": "",
            "type": "object"
        }
        obj.update(opt)
        return obj

    def default_zone(self, **opt):
        zone = {
            "name": "",
            "id": "",
            "contents": [],
            "description": "",
            "events": {},
            "paths": {
                "north": {"lock": False, "target": "", "description": "", "key": ""},
                "west": {"lock": False, "target": "", "description": "", "key": ""},
                "east": {"lock": False, "target": "", "description": "", "key": ""},
                "south": {"lock": False, "target": "", "description": "", "key": ""},
                "up": {"lock": False, "target": "", "description": "", "key": ""},
                "down": {"lock": False, "target": "", "description": "", "key": ""},
                "in": {"lock": False, "target": "", "description": "", "key": ""},
                "out": {"lock": False, "target": "", "description": "", "key": ""},
            },
            "type": "zone"
        }
        zone.update(opt)
        return zone

class Fyction:
    def __init__(self):
        self.statics = FyctionStatic
        self.manager = StateManager(self)
        self.app_path = APP_PATH
        self.scene = SceneManager(self)
        self.reset()

    def reset(self):
        self.game_id = ""
        self.state = {} #The current structure of the world
        self.functions = {} #Functions available for the state to access
        self.events = {} #A dictionary of events for the state to track
        self.commands = {} #Commands that the user executes to interact with the state
        self.aliases = {} #A map of aliases that translate to full commands (e.g. n > move north)
        self.buffer = [] #Text buffer for the state
        self.override = None #Redirect command input to an arbitrary function

        self.manager.new()
        self.config = {}
        self.libraries = {}

        self.load_config()

    def update_path(self):
        sys.path.append(self.app_path+"lib/")

    def load_config(self):
        with open(self.app_path+"fyction.json", "r+") as f:
            self.config = json.load(f)

    def save_config(self):
        with open(self.app_path+"fyction.json", "w+") as f:
            f.write(json.dumps(self.config))

    def focus(self, on_object):
        player = self.manager.get_object(on_object)
        if player != None:
            self.set_var("__player", player["id"])

    def current(self):
        player = self.manager.get_object(self.get_var("__player"))
        location = self.manager.get_any(player["location"])
        return player, location

    def set_command(self, name, fn):
        self.commands[name] = fn

    def set_var(self, key, value): self.state["variables"][key] = value
    def get_var(self, key, default = None): return self.state["variables"].get(key, default)

    def set_conf(self, key, value): self.config[key] = value
    def get_conf(self, key, default = None): return self.config.get(key, default)

    def set_function(self, name, fn): 
        self.functions[name] = fn

    def call_function(self, name, **args):
        if self.functions.get(name, "") != "": return self.functions[name](self, **args)

    def on(self, event_name, function_name):
        """
        Adds a new event.
        function_name must be the name of a function stored in the state.
        """
        if(self.events.get(event_name, "") == ""): self.events[event_name] = []
        self.events[event_name].append(function_name)
        #print(self.events)

    def emit(self, event_name, **args):
        #print("Emit "+event_name)
        if self.events.get(event_name, "") != "":
            #print("List found")
            for event in self.events[event_name]:
                #print("Emitting "+str(event))
                if event in self.functions.keys():
                    self.call_function(event, **args)
                
                if callable(event):
                    event(self, **args)

    def load_state(self, path):
        with open(path, "r")  as f: self.state = json.load(f)

    def set_override(self, fn = None):
        self.override = fn

    def handle_aliases(self, line):
        if line in self.config.get("aliases", {}).keys():
            return self.config['aliases'][line]
        parts = line.split(" ")
        i = 0
        for part in parts:
            if part.startswith("$") and part[1:] in self.config.get("aliases", {}).keys():
                parts[i] = self.config['aliases'][part[1:]]
            i += 1
        return " ".join(parts)

    def handle_removes(self, line):
        removes = self.config.get("removes", [])
        parts = line.split(" ")

        for r in removes:
            while r in parts:
                parts.remove(r)
                
        return " ".join(parts)

    def read(self, line, bypass_override = False):
        if "||" in line:
            lines = line.split("||")
            for new_line in lines: self.read(new_line.strip())
            return
            
        line = self.handle_removes(line)
        line = self.handle_aliases(line)

        if self.override != None and bypass_override == False:
            self.override(self, line)
            return

        cmd = line.split(" ")[0].lower().replace(" ", "")
        ln = " ".join(line.split(" ")[1:])
        if self.commands.get(cmd, None) != None:
            self.emit("tick")
            return self.commands[cmd](self, ln)

        self.writeln("What?")

    def writeln(self, text, source = "world"):
        self.buffer.append({"source": str(source), "text": str(text)})

    def echo(self):
        for bf in self.buffer:
            print(bf["source"]+': '+bf['text'])
        
    def clear_buffer(self):
        self.buffer.clear()

    def load(self, path, sub_directory = "games"):
        self.reset()
        if not path.endswith(".json"): path = path+".json"
        if not os.path.exists(self.app_path+"/"+sub_directory+"/"+path):
            print("Invalid state path: ")
            print(self.app_path+"/"+sub_directory+"/"+path)
            return
        with open(self.app_path+"/"+sub_directory+"/"+path, "r") as f:
            self.state = json.load(f)

        self.game_id = self.state["id"]
        self.import_libraries()

        if self.state["intro"] != "":
            self.writeln(self.state["intro"])

        

    def link_library(self, name):
        if name not in self.state["libraries"]:
            self.state["libraries"].append(name)
            self.import_library(name)

    def unlink_library(self, name):
        if name in self.state["libraries"]:
            self.state["libraries"].remove(name)

    def import_libraries(self):
        for lib in self.state["libraries"]:
            if lib not in self.libraries.keys():
                self.import_library(lib)

    def import_library(self, name):
        if name in self.libraries.keys():
            return
        f = importlib.import_module("lib."+name)
        f.load(self)
        self.libraries[name] = f

    def export(self, path = "", sub_directory = "games"):
        if path == "": path = self.game_id
        if path == "":
            return print("No game_id")
        self.game_id = path.split(".")[0]
        if not path.endswith(".json"): path = path+".json"
        with open(self.app_path+"/"+sub_directory+"/"+path, "w+") as f:
            f.write(json.dumps(self.state, indent=4))
